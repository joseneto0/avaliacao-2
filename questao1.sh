#!/bin/bash

echo "Calculadora: "
echo "1 - Soma"
echo "2 - Subtração"
echo "3 - Multiplicação"
echo "4 - Divisão"
echo "5 - Raiz Quadrada"
read operacao
[ $operacao -gt 5 ] && echo "Coloque uma opção válida na prox vez :)" && exit 1
[ $operacao -lt 1 ] && echo "Coloque uma opção válida na prox vez :)" && exit 1
[ $operacao -eq 5 ] && echo "Número: " || echo "Primeiro Número: "
read num1
[ $operacao -eq 5 ] && echo "Raiz quadrada: " 
[ $operacao -eq 5 ] && echo "sqrt($num1)" | bc && exit 0
echo "Segundo Número: "
read num2
[ $num2 -eq 0 ] && [ $operacao -eq 4 ] && echo "Divisão por zero, não vai dar :c" && exit 0
soma=$(echo "$num1 + $num2" | bc)
[ $operacao -eq 1 ] && echo "Soma: $soma"
subtracao=$(echo "$num1 - $num2" | bc)
[ $operacao -eq 2 ] && echo "Subtração: $subtracao"
multiplicacao=$(echo "$num1 * $num2" | bc)
[ $operacao -eq 3 ] && echo "Multiplicação: $multiplicacao"
divisao=$(echo "$num1 / $num2" | bc)
[ $operacao -eq 4 ] && echo "Divisão: $divisao"

