#!/bin/bash

echo "Modificação da Variavel PS1"

echo "Deseja voltar ao original?"
echo "1 - Sim"
echo "0 - Não"
read escolha
[ $escolha -eq 1 ] && echo $PS1 && exit 0
echo "Que cor você quer?"
echo "1 - Azul"
read cor
echo "Qual informação você quer exibir? "
echo "1 - Usuário"
echo "2 - Host"
echo "3 - Diretório"
read informacao
[ $informacao -eq 1 ] && [ $cor -eq 1 ] && PS1="\[provinha@\u\]\$ \e[4m"
[ $informacao -eq 2 ] && [ $cor -eq 1 ] && PS1="\[provinha@\h\]\$ \e[4m"
[ $informacao -eq 3 ] && [ $cor -eq 1 ] && PS1="\[provinha@\W\]\$ \e[4m"
